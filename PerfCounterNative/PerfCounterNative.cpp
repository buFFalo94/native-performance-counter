#include "PerfCounterNative.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "sys/times.h"
#include "sys/vtimes.h"

static clock_t lastCPU, lastSysCPU, lastUserCPU;
static int numProcessors;
static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;
struct sysinfo memInfo;

static  int parseLine(char* line) 
{
	// This assumes that a digit will be found and the line ends in " Kb".
	int i = strlen(line);
	const char* p = line;
	while (*p <'0' || *p > '9') p++;
	line[i - 3] = '\0';
	i = atoi(p);
	return i;
}

extern "C"
{
	__attribute__((visibility("default"))) void Initialize()
	{
		FILE* file = fopen("/proc/stat", "r");
		fscanf(file,
			"cpu %llu %llu %llu %llu",
			&lastTotalUser,
			&lastTotalUserLow,
			&lastTotalSys,
			&lastTotalIdle);
		fclose(file);
		
		FILE* _file;
		struct tms timeSample;
		char line[128];

		lastCPU = times(&timeSample);
		lastSysCPU = timeSample.tms_stime;
		lastUserCPU = timeSample.tms_utime;

		_file = fopen("/proc/cpuinfo", "r");
		numProcessors = 0;
		while (fgets(line, 128, _file) != NULL) {
			if (strncmp(line, "processor", 9) == 0) numProcessors++;
		}
		fclose(_file);
	}
	__attribute__((visibility("default"))) long GetTotalVirtualRAM()
	{
		sysinfo(&memInfo);
		long long totalVirtualMem = memInfo.totalram;
		//Add other values in next statement to avoid int overflow on right hand side...
		totalVirtualMem += memInfo.totalswap;
		totalVirtualMem *= memInfo.mem_unit;
		return totalVirtualMem;
	}
	__attribute__((visibility("default"))) long GetUsedVirtualRAM()
	{
		sysinfo(&memInfo);
		long long virtualMemUsed = memInfo.totalram - memInfo.freeram;
		//Add other values in next statement to avoid int overflow on right hand side...
		virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
		virtualMemUsed *= memInfo.mem_unit;
		return virtualMemUsed;
	}
	int GetProcUsedVirtualRAM()
	{
		FILE* file = fopen("/proc/self/status", "r");
		int result = -1;
		char line[128];

		while (fgets(line, 128, file) != NULL) {
			if (strncmp(line, "VmSize:", 7) == 0) {
				result = parseLine(line);
				break;
			}
		}
		fclose(file);
		return result;
	}
	__attribute__((visibility("default"))) long GetTotalRAM()
	{
		sysinfo(&memInfo);
		long long totalPhysMem = memInfo.totalram;
		//Multiply in next statement to avoid int overflow on right hand side...
		totalPhysMem *= memInfo.mem_unit;
		return totalPhysMem;
	}
	__attribute__((visibility("default"))) long GetUsedRAM()
	{
		sysinfo(&memInfo);
		long long physMemUsed = memInfo.totalram - memInfo.freeram;
		//Multiply in next statement to avoid int overflow on right hand side...
		physMemUsed *= memInfo.mem_unit;
		return physMemUsed;
	}
	__attribute__((visibility("default"))) int GetProcUsedRAM()
	{
		// Note : this value is in KB!
		FILE* file = fopen("/proc/self/status", "r");
		int result = -1;
		char line[128];

		while (fgets(line, 128, file) != NULL) 
		{
			if (strncmp(line, "VmRSS:", 6) == 0) 
			{
				result = parseLine(line);
				break;
			}
		}
		fclose(file);
		return result;
	}
	__attribute__((visibility("default"))) double GetCPUUsage()
	{
		double percent;
		FILE* file;
		unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;

		file = fopen("/proc/stat", "r");
		fscanf(file,
			"cpu %llu %llu %llu %llu",
			&totalUser,
			&totalUserLow,
			&totalSys,
			&totalIdle);
		fclose(file);

		if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow || totalSys < lastTotalSys || totalIdle < lastTotalIdle) 
		{
			//Overflow detection. Just skip this value.
			percent = -1.0;
		}
		else 
		{
			total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) + (totalSys - lastTotalSys);
			percent = total;
			total += (totalIdle - lastTotalIdle);
			percent /= total;
			percent *= 100;
		}

		lastTotalUser = totalUser;
		lastTotalUserLow = totalUserLow;
		lastTotalSys = totalSys;
		lastTotalIdle = totalIdle;

		return percent;
	}
	__attribute__((visibility("default"))) double GetProcCPUUsage()
	{
		struct tms timeSample;
		clock_t now;
		double percent;

		now = times(&timeSample);
		if (now <= lastCPU || timeSample.tms_stime < lastSysCPU ||
		    timeSample.tms_utime < lastUserCPU) {
			//Overflow detection. Just skip this value.
			percent = -1.0;
		}
		else {
			percent = (timeSample.tms_stime - lastSysCPU) +
			    (timeSample.tms_utime - lastUserCPU);
			percent /= (now - lastCPU);
			percent /= numProcessors;
			percent *= 100;
		}
		lastCPU = now;
		lastSysCPU = timeSample.tms_stime;
		lastUserCPU = timeSample.tms_utime;

		return percent;
	}
}